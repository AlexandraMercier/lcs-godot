extends Control

signal new_random_slogan(slogan)

export(Resource) var liberal_things
export(Resource) var conservative_things
export(Resource) var base_sentences


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_AutoGenerateButton_pressed():
	var liberal_thing = liberal_things.random_word()
	var base_slogan = base_sentences.random_word()
	var conservative_thing = conservative_things.random_word()

	var slogan = base_slogan.format({
		"lib": liberal_thing,
		"con": conservative_thing
		})
		
	emit_signal("new_random_slogan", slogan)
