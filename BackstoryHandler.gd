extends Node

export (Resource) var backstory_tree

const default_backstory = {
	"base": {
		"dialog" : "I was born a ...",
		"choices": ["girl", "boy", "intersex"]
	}
}

var backstory

var baseAnswerScene = preload("res://DialogAnswerButton.tscn")

onready var dialogText = $ColorRect/VBoxContainer/RichTextLabel
onready var possibleAnswers = $ColorRect/VBoxContainer/DialogChoices


# Called when the node enters the scene tree for the first time.
func _ready():
	backstory = default_backstory
	
	dialogText.text = backstory["base"]["dialog"]
	for choice in backstory["base"]["choices"]:
		var answer = baseAnswerScene.instance()
		answer.text = choice
		possibleAnswers.add_child(answer)
		answer.connect("choice_selected", self, "_on_choice_selected")
		
func _on_choice_selected(answerText):
	print(answerText)
