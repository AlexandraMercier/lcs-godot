extends Node

export(Dictionary) var dialog = {
	"base": {
		"dialog" : "I was born a ...",
		"choices": ["girl", "boy", "intersex"]
	}
}
