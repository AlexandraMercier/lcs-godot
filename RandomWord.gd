extends Resource
class_name RandomWord

export(Array) var word_list = []

func random_word():
	var rand_index = randi() % word_list.size()
	return word_list[rand_index]
